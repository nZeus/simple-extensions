# Simple Extensions

SimpleExtensions is a small library that contains extension methods to parse primitive types and do simple every-day stuff

## Documentation

The detailed documentation is located [here](http://nzeus.gitlab.io/simple-extensions/).

A few examples:

``` csharp
int result = "1".ToInt();                   // 1
int result = "1".ToInt(12);                 // 1
int result = "".ToInt(12);                  // 12

int? result = "1".ToIntNullable();          // 1
int? result = "".ToIntNullable();           // null
int? result = "".ToIntNullable(12);         // 12

decimal result = "1 333.2".ToDecimal();     // 1333.2f
decimal result = "1 333,2".ToDecimal();     // 1333.2f
decimal result = "1 333,2".ToDecimal(".");  // InvalidCastException
decimal result = "".ToDecimal(123.45);      // 123.45f

TestEnum result = "Two".ToEnum<TestEnum>(); // TestEnum.Two
TestEnum result = "Bla".ToEnum<TestEnum>(TestEnum.Two); // TestEnum.Two
TestEnum? result = "Bla".ToEnumNullable<TestEnum>(); // null

string result = (new[] {"First", "Second"}).JoinWith(", "); // "First, Second"
```

## Development

The documentation is build and updated automatically with the help of ~~[Sandcastle Help File Builder](https://github.com/EWSoftware/SHFB/releases/tag/v2016.9.17.0)~~. To compile the documentation locally:

* Restore dotnet tools `dotnet tool restore`
* Run `dotnet tool run wyam build`
* Run `dotnet tool run wyam preview` to preview the result

## License

SimpleExtensions is licensed under [MIT License](http://www.opensource.org/licenses/MIT).
