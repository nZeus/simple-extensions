Title: SimpleExtensions
---

SimpleExtensions is a small library that contains extension methods to parse primitive types and do simple every-day stuff.

Repository: https://gitlab.com/nZeus/simple-extensions

A few examples:

``` csharp
int result = "1".ToInt();                   // 1
int result = "1".ToInt(12);                 // 1
int result = "".ToInt(12);                  // 12

int? result = "1".ToIntNullable();          // 1
int? result = "".ToIntNullable();           // null
int? result = "".ToIntNullable(12);         // 12

decimal result = "1 333.2".ToDecimal();     // 1333.2f
decimal result = "1 333,2".ToDecimal();     // 1333.2f
decimal result = "1 333,2".ToDecimal(".");  // InvalidCastException
decimal result = "".ToDecimal(123.45);      // 123.45f

TestEnum result = "Two".ToEnum<TestEnum>(); // TestEnum.Two
TestEnum result = "Bla".ToEnum<TestEnum>(TestEnum.Two); // TestEnum.Two
TestEnum? result = "Bla".ToEnumNullable<TestEnum>(); // null

string result = (new[] {"First", "Second"}).JoinWith(", "); // "First, Second"
```
