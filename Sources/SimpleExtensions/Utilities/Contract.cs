﻿using System;

namespace SimpleExtensions.Utilities
{
    internal class Contract
    {
        public static void Requires<TException>(bool predicate, string message)
            where TException : Exception, new()
        {
            if (predicate)
            {
                return;
            }

            if (typeof(TException) == typeof(ArgumentException))
            {
                throw new ArgumentException(message);
            }

            throw new TException();
        }
    }
}
