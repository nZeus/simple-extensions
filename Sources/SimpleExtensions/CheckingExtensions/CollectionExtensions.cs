using System.Collections.Generic;
using System.Linq;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify validation of the IEnumerable objects
    /// </summary>
    public static partial class CollectionExtensions
    {
        /// <summary>
        /// 	<para>Verifies if collection is null or empty</para>
        /// </summary>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }

        /// <summary>
        /// 	<para>Verifies if collection is not null and not empty</para>
        /// </summary>
        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return !source.IsNullOrEmpty();
        }
    }
}
