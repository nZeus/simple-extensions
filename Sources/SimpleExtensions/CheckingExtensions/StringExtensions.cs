﻿namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify validation of the String objects
    /// </summary>
    public static partial class StringExtensions
    {
        /// <summary>
        /// 	<para>Verifies if string is null or empty</para>
        /// </summary>
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// 	<para>Verifies if string is not null and not empty</para>
        /// </summary>
        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// 	<para>Verifies if string is null or whitespace</para>
        /// </summary>
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        /// <summary>
        /// 	<para>Verifies if string is not null and not whitespace</para>
        /// </summary>
        public static bool IsNotNullOrWhiteSpace(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }
    }
}
