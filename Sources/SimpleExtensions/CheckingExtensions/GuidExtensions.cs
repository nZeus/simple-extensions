﻿using System;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify validation of the Guid objects
    /// </summary>
    public static partial class GuidExtensions
    {
        /// <summary>
        /// 	<para>Verifies if object is null or empty</para>
        /// </summary>
        public static bool IsNullOrEmpty(this Guid? source)
        {
            return source == null || source.Value == Guid.Empty;
        }

        /// <summary>
        /// 	<para>Verifies if object is not null and not empty</para>
        /// </summary>
        public static bool IsNotNullOrEmpty(this Guid? source)
        {
            return !IsNullOrEmpty(source);
        }

        /// <summary>
        /// 	<para>Verifies if object is empty</para>
        /// </summary>
        public static bool IsEmpty(this Guid source)
        {
            return source == Guid.Empty;
        }

        /// <summary>
        /// 	<para>Verifies if object is not empty</para>
        /// </summary>
        public static bool IsNotEmpty(this Guid source)
        {
            return !IsEmpty(source);
        }
    }
}
