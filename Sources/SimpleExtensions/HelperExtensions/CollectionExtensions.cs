using System.Collections;

namespace SimpleExtensions
{
    /// <summary>
    /// Extension class for checking object parameters
    /// </summary>
    public static partial class CollectionExtensions
    {
        /// <summary>
        /// 	<para>Compares two arrays</para>
        /// </summary>
        /// <param name="array1">The first array to compare</param>
        /// <param name="array2">The second array to compare</param>
        public static bool Compare<T>(this T[] array1, T[] array2)
        {
            return ((IStructuralEquatable) array1).Equals(array2, StructuralComparisons.StructuralEqualityComparer);
        }
    }
}
