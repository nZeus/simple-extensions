﻿using System.Collections.Generic;

namespace SimpleExtensions
{
    /// <summary>
    /// Extension class for checking object parameters
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Gets the value from a dictionary in a safe way.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   Null in case the key was not found in the dictionary.
        ///   TValue in case the key was found in the dictionary.
        /// </returns>
        public static TValue? GetValueSafe<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key)
            where TValue: struct
        {
            if (source.TryGetValue(key, out TValue result))
            {
                return result;
            }

            return null;
        }

        /// <summary>
        /// Gets the value from a dictionary in a safe way.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        ///   <paramref name="defaultValue" /> or <c>null</c> in case the key was not found in the dictionary.
        ///   TValue in case the key was found in the dictionary.
        /// </returns>
        public static TValue GetValueSafe<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key, TValue @defaultValue = null)
            where TValue: class
        {
            if (source.TryGetValue(key, out TValue result))
            {
                return result;
            }

            return @defaultValue;
        }
    }
}
