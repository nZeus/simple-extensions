using System.Collections.Generic;

namespace SimpleExtensions
{
    /// <summary>
    /// Extension methods for string collection
    /// </summary>
    public static partial class StringCollectionExtensions
    {
        /// <summary>
        /// 	<para>Concatenates all the elements of a string array, using the specified separator between each element</para>
        /// </summary>
        /// <param name="array">The string array to transform</param>
        public static string JoinWith(this IEnumerable<string> array, string separator)
        {
            return string.Join(separator, array);
        }
    }
}
