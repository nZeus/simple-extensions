using System;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to Boolean
    /// </summary>
    public static class BooleanExtensions
    {
        /// <summary>
        ///		<para>Tries to cast an object to a boolean.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a boolean</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static bool ToBool(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "BooleanExtensions.ToBool() failed because value is null");

            try
            {
                return Convert.ToBoolean(value);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"BooleanExtensions.ToBool() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to a boolean.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a boolean</returns>
        public static bool ToBool(this object value, bool defaultValue)
        {
            try
            {
                return value == null ? defaultValue : Convert.ToBoolean(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to a nullable bool.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to bool.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a boolean or null</returns>
        public static bool? ToBoolNullable(this object value)
        {
            try
            {
                return value == null ? (bool?)null : Convert.ToBoolean(value);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
