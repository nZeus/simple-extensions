using System;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to integer types
    /// </summary>
    public static class IntegerExtensions
    {
        /// <summary>
        ///		<para>Tries to cast an object to a short.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a short value</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static short ToInt16(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "IntegerExtensions.ToInt16() failed because value is null");

            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"IntegerExtensions.ToInt16() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to a short.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a short value</returns>
        public static short ToInt16(this object value, short defaultValue)
        {
            try
            {
                return value == null ? defaultValue : Convert.ToInt16(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to a nullable short.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to short.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an short.= value or null</returns>
        public static short? ToInt16Nullable(this object value)
        {
            try
            {
                return value == null ? (short?)null : Convert.ToInt16(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to an integer.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an integer value</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static int ToInt(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "IntegerExtensions.ToInt() failed because value is null");

            try
            {
                return Convert.ToInt32(value);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"IntegerExtensions.ToInt() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to an integer.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns an integer value</returns>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static int ToInt(this object value, int defaultValue)
        {
            try
            {
                return value == null ? defaultValue : Convert.ToInt32(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to a nullable integer.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to int.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an integer value or null</returns>
        public static int? ToIntNullable(this object value)
        {
            try
            {
                return value == null ? (int?)null : Convert.ToInt32(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to a long.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a long</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static long ToInt64(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "IntegerExtensions.ToInt64() failed because value is null");

            try
            {
                return Convert.ToInt64(value);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"IntegerExtensions.ToInt64() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to a long.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a long</returns>
        public static long ToInt64(this object value, long defaultValue)
        {
            try
            {
                return value == null ? defaultValue : Convert.ToInt64(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to a nullable long.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to long.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a long or null</returns>
        public static long? ToInt64Nullable(this object value)
        {
            try
            {
                return value == null ? (long?)null : Convert.ToInt64(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to an unsigned integer.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an unsigned integer</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static uint ToUInt(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "IntegerExtensions.ToUInt() failed because value is null");

            try
            {
                return Convert.ToUInt32(value);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"IntegerExtensions.ToUInt() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to an unsigned integer.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns an unsigned integer</returns>
        public static uint ToUInt(this object value, uint defaultValue)
        {
            try
            {
                return value == null ? defaultValue : Convert.ToUInt32(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to an unsigned int nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to unsigned int.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an unsigned integer or null</returns>
        public static uint? ToUIntNullable(this object value)
        {
            try
            {
                return value == null ? (uint?)null : Convert.ToUInt32(value);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
