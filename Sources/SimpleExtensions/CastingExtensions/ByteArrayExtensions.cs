using System.IO;
using System.Text;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to/from a byte array
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// Converts a byte array to a memory stream
        /// </summary>
        /// <param name="value">The byte array to convert</param>
        /// <returns></returns>
        public static MemoryStream ToMemoryStream(this byte[] value)
        {
            var stream = new MemoryStream();

            if (value == null)
            {
                return stream;
            }

            //TODO: write by 1024 bytes
            stream.Write(value, 0, value.Length);
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        /// <summary>
        /// Converts a memory stream to a byte array
        /// </summary>
        /// <param name="stream">The stream to convert</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this MemoryStream stream)
        {
            if (stream == null)
            {
                return new byte[0];
            }

            var bytes = new byte[stream.Length];
            if (stream.CanSeek)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }

            //TODO: read by 1024 bytes
            stream.Read(bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// Converts a byte array to a string
        /// </summary>
        /// <param name="value">The byte array to convert</param>
        /// <param name="encoding">The encoding to use during the convertion</param>
        /// <returns></returns>
        public static string ToString(this byte[] value, Encoding encoding)
        {
            if (value == null)
            {
                return null;
            }

            return encoding.GetString(value);
        }

        /// <summary>
        /// Converts a string to a byte array
        /// </summary>
        /// <param name="value">The string to convert</param>
        /// <param name="encoding">The encoding to use during the convertion</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this string value, Encoding encoding)
        {
            if (value.IsNullOrEmpty())
            {
                return new byte[0];
            }

            return encoding.GetBytes(value);
        }
    }
}
