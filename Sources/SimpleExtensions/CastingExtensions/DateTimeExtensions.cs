using System;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to DateTime
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        ///		<para>Tries to cast an object to a DateTime.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a DateTime</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static DateTime ToDateTime(this object value)
        {
            Contract.Requires<ArgumentNullException>(value != null, "DateTimeExtensions.ToDateTime() failed because value is null");

            try
            {
                if (value is DateTime)
                {
                    return (DateTime) value;
                }

                return DateTime.Parse(value.ToString());
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"DateTimeExtensions.ToDateTime() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Tries to cast an object to a DateTime.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a DateTime</returns>
        public static DateTime ToDateTime(this object value, DateTime defaultValue)
        {
            try
            {
                if (value is DateTime)
                {
                    return (DateTime) value;
                }

                return value == null ? defaultValue : DateTime.Parse(value.ToString());
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///		<para>Casts an object to a DateTime nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to a DateTime.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a DateTime or null</returns>
        public static DateTime? ToDateTimeNullable(this object value)
        {
            try
            {
                if (value is DateTime)
                {
                    return (DateTime)value;
                }

                return value == null ? (DateTime?)null : DateTime.Parse(value.ToString());
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
