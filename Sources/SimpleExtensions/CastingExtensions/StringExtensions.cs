using System;
using System.Linq;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to string
    /// </summary>
    public static partial class StringExtensions
    {
        /// <summary>
        ///		<para>Tries to cast an object to a char.</para>
        ///		<para>Never throws an exception when <paramref name="defaultValue"/> is specified.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast and <paramref name="defaultValue"/> specified.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a char</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static char ToChar(this object value, char? defaultValue = null)
        {
            Contract.Requires<ArgumentNullException>(defaultValue != null || value != null, "StringExtensions.ToChar() failed because value is null");

            try
            {
                return value == null ? defaultValue.Value : char.Parse(value.ToString());
            }
            catch (Exception exception)
            {
                if (defaultValue != null)
                {
                    return defaultValue.Value;
                }

                throw new InvalidCastException($"StringExtensions.ToChar() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        /// Returns non-nullable string. It never throws an exception
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a string even if <paramref name="value"/> is <c>null</c></returns>
        public static string ToStringSafe(this object? value)
        {
            try
            {
                return value?.ToString() ?? string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Represents byte array as a hex string
        /// </summary>
        /// <param name="array">The byte array to convert</param>
        /// <returns>Returns a string in the HEX format (e.g. F0A166FF)</returns>
        public static string ToHexString(this byte[] array)
        {
            try
            {
                return string.Join("", array.Select(x => $"{x:X2}"));
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
