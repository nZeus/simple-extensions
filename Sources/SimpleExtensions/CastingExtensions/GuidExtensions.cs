﻿using System;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to Guid
    /// </summary>
    public static partial class GuidExtensions
    {
        /// <summary>
        ///		<para>Tries to cast an object to a Guid.</para>
        ///		<para>Never throws an exception when <paramref name="defaultValue"/> is specified.</para>
        ///		<para>Returns <paramref name="defaultValue"/> when cannot cast and <paramref name="defaultValue"/> specified.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a GUID</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static Guid ToGuid(this object value, Guid? defaultValue = null)
        {
            Contract.Requires<ArgumentNullException>(defaultValue != null || value != null, "GuidExtensions.ToGuid() failed because value is null");

            try
            {
                if (value is Guid)
                {
                    return (Guid)value;
                }
                
                return value == null ? defaultValue.Value : new Guid(value.ToString());
            }
            catch (Exception exception)
            {
                if (defaultValue != null)
                {
                    return defaultValue.Value;
                }

                throw new InvalidCastException($"GuidExtensions.ToGuid() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        ///		<para>Casts an object to a Guid nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to a Guid.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a GUID or null</returns>
        public static Guid? ToGuidNullable(this object value)
        {
            try
            {
                if (value == null)
                {
                    return null;
                }

                var guid = new Guid(value.ToString());
                if (guid == Guid.Empty)
                {
                    return null;
                }
                return guid;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
