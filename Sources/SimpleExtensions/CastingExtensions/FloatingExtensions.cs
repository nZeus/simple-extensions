using System;
using System.Globalization;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to float/decimal/double
    /// </summary>
    public static class FloatingExtensions
    {
        /// <summary>
        /// 	<para>Tries to cast an object to a decimal.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <returns>Returns a decimal</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static decimal ToDecimal(this object value, string? numberDecimalSeparator = null)
        {
            Contract.Requires<ArgumentNullException>(value != null, "FloatingExtensions.ToDecimal() failed because value is null");

            try
            {
                if (value is decimal)
                {
                    return (decimal)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return Convert.ToDecimal(value, numberFormatInfo);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"FloatingExtensions.ToDecimal() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a decimal.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a decimal</returns>
        public static decimal ToDecimal(this object value, string? numberDecimalSeparator, decimal defaultValue)
        {
            try
            {
                if (value is decimal)
                {
                    return (decimal)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return value == null ? defaultValue : Convert.ToDecimal(value, numberFormatInfo);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a decimal. Will try to guess decimal separator.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast and <paramref name="defaultValue"/> specified.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a decimal</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static decimal ToDecimal(this object value, decimal defaultValue)
        {
            return ToDecimal(value, null, defaultValue);
        }

        /// <summary>
        ///		<para>Casts an object to a decimal nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to a decimal.</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Returns a decimal or null</returns>
        public static decimal? ToDecimalNullable(this object value)
        {
            try
            {
                return value == null ? (decimal?)null : ToDecimal(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a float.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <returns>Returns a float</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static float ToFloat(this object value, string? numberDecimalSeparator = null)
        {
            Contract.Requires<ArgumentNullException>(value != null, "FloatingExtensions.ToFloat() failed because value is null");

            try
            {
                if (value is float)
                {
                    return (float)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return Convert.ToSingle(value, numberFormatInfo);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"FloatingExtensions.ToFloat() failed for the value: [{value}]", exception);
            }
        }


        /// <summary>
        /// 	<para>Tries to cast an object to a float.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a float</returns>
        public static float ToFloat(this object value, string? numberDecimalSeparator, float defaultValue)
        {
            try
            {
                if (value is float)
                {
                    return (float)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return value == null ? defaultValue : Convert.ToSingle(value, numberFormatInfo);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a float.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a float</returns>
        public static float ToFloat(this object value, float defaultValue)
        {
            return ToFloat(value, null, defaultValue);
        }

        /// <summary>
        ///		<para>Casts an object to float nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to a float.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a float or null</returns>
        public static float? ToFloatNullable(this object value)
        {
            try
            {
                return value == null ? (float?) null : ToFloat(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a double.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <returns>Returns a double</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="value"/> is <c>null</c></exception>
        /// <exception cref="InvalidCastException">Thrown when cannot do conversion</exception>
        public static double ToDouble(this object value, string? numberDecimalSeparator = null)
        {
            Contract.Requires<ArgumentNullException>(value != null, $"FloatingExtensions.ToDouble() failed because ${nameof(value)} is null");

            try
            {
                if (value is double)
                {
                    return (double)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return Convert.ToDouble(value, numberFormatInfo);
            }
            catch (Exception exception)
            {
                throw new InvalidCastException($"FloatingExtensions.ToDouble() failed for the value: [{value}]", exception);
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a double.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="numberDecimalSeparator">Decimal separator used during the conversion. If <c>null</c> then attempt to guess the separator will be made</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a double</returns>
        public static double ToDouble(this object value, string? numberDecimalSeparator, double defaultValue)
        {
            try
            {
                if (value is double)
                {
                    return (double)value;
                }

                var numberFormatInfo = new NumberFormatInfo
                {
                    NumberDecimalSeparator = numberDecimalSeparator ?? GuessDecimalSeparator(value.ToStringSafe()),
                    NumberGroupSeparator = " "
                };

                return value == null ? defaultValue : Convert.ToDouble(value, numberFormatInfo);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 	<para>Tries to cast an object to a double.</para>
        /// 	<para>Never throws an exception.</para>
        /// 	<para>Returns <paramref name="defaultValue"/> when cannot cast.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns a double</returns>
        public static double ToDouble(this object value, double defaultValue)
        {
            return ToDouble(value, null, defaultValue);
        }

        /// <summary>
        ///		<para>Casts an object to double nullable.</para>
        ///		<para>Never throws an exception.</para>
        ///		<para>Returns null when cannot cast to a double.</para>
        /// </summary>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns a double or null</returns>
        public static double? ToDoubleNullable(this object value)
        {
            try
            {
                return value == null ? (double?)null : ToDouble(value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string GuessDecimalSeparator(string value)
        {
            var containsComma = value.IndexOf(',') > 0;

            if (value.IndexOf('.') > 0)
            {
                if (containsComma)
                {
                    throw new Exception("Decimal cannot contain comma and dot at the same time");
                }

                return ".";
            }

            if (containsComma)
            {
                return ",";
            }

            return CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        }
    }
}
