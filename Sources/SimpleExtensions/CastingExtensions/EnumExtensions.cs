using System;
using SimpleExtensions.Utilities;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to enum
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 	<para>Casts a string to an Enum</para>
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an enum</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static T ToEnum<T>(this object value)
            where T : struct
        {
            Contract.Requires<ArgumentNullException>(value != null, "EnumExtensions.ToEnum() failed because value is null");

            if (Enum.TryParse(value?.ToString(), ignoreCase: true, out T result))
            {
                return result;
            }

            throw new InvalidCastException($"EnumExtensions.ToEnum() failed for the value: [{value}]");
        }

        /// <summary>
        /// 	<para>Casts a string to an Enum</para>
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns an enum</returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static T ToEnum<T>(this object value, T defaultValue)
            where T : struct
        {
            if (Enum.TryParse(value?.ToString(), ignoreCase: true, out T result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        /// 	<para>Casts a string to an Enum</para>
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value">The object to convert</param>
        /// <returns>Returns an enum</returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static T ToEnumNullable<T>(this object value)
            where T : struct
        {
            Contract.Requires<ArgumentNullException>(value != null, "EnumExtensions.ToEnum() failed because value is null");

            if (Enum.TryParse(value?.ToString(), ignoreCase: true, out T result))
            {
                return result;
            }

            throw new InvalidCastException($"EnumExtensions.ToEnum() failed for the value: [{value}]");
        }

        /// <summary>
        /// 	<para>Casts a string to an Enum</para>
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="value">The object to convert</param>
        /// <param name="defaultValue">The default value if the object cannot be converted</param>
        /// <returns>Returns an enum</returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="defaultValue"/> is not specified and <paramref name="value"/> is null</exception>
        /// <exception cref="InvalidCastException">Thrown when <paramref name="defaultValue"/> is not specified and cannot do conversion</exception>
        public static T ToEnumNullable<T>(this object value, T? defaultValue = null)
            where T : struct
        {
            Contract.Requires<ArgumentNullException>(defaultValue != null || value != null, "EnumExtensions.ToEnum() failed because value is null");

            if (Enum.TryParse(value?.ToString(), ignoreCase: true, out T result))
            {
                return result;
            }

            if (defaultValue != null)
            {
                return defaultValue.Value;
            }

            throw new InvalidCastException($"EnumExtensions.ToEnum() failed for the value: [{value}]");
        }
    }
}
