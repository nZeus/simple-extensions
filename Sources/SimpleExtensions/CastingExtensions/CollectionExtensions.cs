using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleExtensions
{
    /// <summary>
    /// Contains extension methods that simplify casting to collections
    /// </summary>
    public static partial class CollectionExtensions
    {
        /// <summary>
        /// 	<para>Casts a list to a grouped dictionary</para>
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="list">The IEnumerable to convert</param>
        /// <param name="groupCriteria">A function to extract a key from each element</param>
        /// <returns></returns>
        public static IDictionary<T1, List<T2>> ToGroupedDictionary<T1, T2>(this IEnumerable<T2> list, Func<T2, T1> groupCriteria)
        {
            return list
                .GroupBy(groupCriteria)
                .ToDictionary(x => x.Key, x => x.ToList());
        }
    }
}
