﻿using Xunit;

namespace SimpleExtensions.Tests.CheckingExtensions
{
    public class StringCollectionExtensionsTests
    {
        [Fact]
        public void Joining_strings_with_a_separator_should_succeed()
        {
            var array = new string[] {
                "first",
                "second"
            };

            var result = array.JoinWith(", ");

            Assert.Equal("first, second", result);
        }

        [Fact]
        public void Joining_string_array_with_a_single_item_should_not_add_a_separator()
        {
            var array = new string[] {
                "first"
            };

            var result = array.JoinWith(", ");

            Assert.Equal("first", result);
        }

        [Fact]
        public void Joining_empty_string_array_should_return_empty_string()
        {
            var array = new string[0];

            var result = array.JoinWith(", ");

            Assert.Equal("", result);
        }
    }
}
