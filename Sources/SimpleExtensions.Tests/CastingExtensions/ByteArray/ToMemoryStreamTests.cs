﻿using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ToMemoryStreamTests
    {
        [Fact]
        public void Casting_byte_array_should_succeed()
        {
            var array = new byte[] {0x01, 0x02, 0x03, 0x04};
            var result = array.ToMemoryStream();

            Assert.Equal(array, result.ToArray());
        }

        [Fact]
        public void Casting_empty_byte_array_should_succeed()
        {
            var array = new byte[0];
            var result = array.ToMemoryStream();

            Assert.Equal(array, result.ToArray());
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var array = new byte[0];
            var result = ((byte[])null).ToMemoryStream();

            Assert.Equal(array, result.ToArray());
        }
    }
}
