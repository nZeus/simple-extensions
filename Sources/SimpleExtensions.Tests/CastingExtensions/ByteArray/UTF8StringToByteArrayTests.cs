using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class UTF8StringToByteArrayTests
    {
        [Fact]
        public void Casting_a_string_should_succeed()
        {
            var result = "\u0444".ToByteArray(Encoding.UTF8);

            var expected = new byte[] { 0xD1, 0x84 };
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_empty_string_should_succeed()
        {
            var result = string.Empty.ToByteArray(Encoding.UTF8);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((string)null).ToByteArray(Encoding.UTF8);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }
    }
}