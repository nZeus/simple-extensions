using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ToUnicodeStringTests
    {
        [Fact]
        public void Casting_byte_array_should_succeed()
        {
            var array = new byte[] {0x44, 0x04};
            var result = array.ToString(Encoding.Unicode);
            
            Assert.Equal("\u0444", result);
        }

        [Fact]
        public void Casting_empty_byte_array_should_succeed()
        {
            var array = new byte[0];

            var result = array.ToString(Encoding.Unicode);

            Assert.Equal(string.Empty, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((byte[])null).ToString(Encoding.Unicode);

            Assert.Null(result);
        }
    }
}