﻿using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ToASCIIStringTests
    {
        [Fact]
        public void Casting_byte_array_should_succeed()
        {
            var array = new byte[] {0x67, 0x70, 0x73, 0x35};

            var result = array.ToString(Encoding.ASCII);
            
            Assert.Equal("gps5", result);
        }

        [Fact]
        public void Casting_empty_byte_array_should_succeed()
        {
            var array = new byte[0];

            var result = array.ToString(Encoding.ASCII);

            Assert.Equal(string.Empty, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((byte[])null).ToString(Encoding.ASCII);

            Assert.Null(result);
        }
    }
}