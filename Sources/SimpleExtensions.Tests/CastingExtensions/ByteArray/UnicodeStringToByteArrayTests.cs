using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class UnicodeStringToByteArrayTests
    {
        [Fact]
        public void Casting_a_string_should_succeed()
        {
            var result = "\u0444".ToByteArray(Encoding.Unicode);

            var expected = new byte[] { 0x44, 0x04 };
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_empty_string_should_succeed()
        {
            var result = string.Empty.ToByteArray(Encoding.Unicode);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((string)null).ToByteArray(Encoding.Unicode);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }
    }
}