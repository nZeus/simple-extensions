using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ToUTF8StringTests
    {
        [Fact]
        public void Casting_byte_array_should_succeed()
        {
            var array = new byte[] {0xD1, 0x84};
            var result = array.ToString(Encoding.UTF8);
            
            Assert.Equal("\u0444", result); // Unicode representation of the same character
        }

        [Fact]
        public void Casting_empty_byte_array_should_succeed()
        {
            var array = new byte[0];

            var result = array.ToString(Encoding.UTF8);

            Assert.Equal(string.Empty, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((byte[])null).ToString(Encoding.UTF8);

            Assert.Null(result);
        }
    }
}