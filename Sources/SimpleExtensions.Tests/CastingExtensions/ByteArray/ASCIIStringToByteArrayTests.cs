using System.Text;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ASCIIStringToByteArrayTests
    {
        [Fact]
        public void Casting_a_string_should_succeed()
        {
            var result = "gps5".ToByteArray(Encoding.ASCII);

            var expected = new byte[] { 0x67, 0x70, 0x73, 0x35 };
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_empty_string_should_succeed()
        {
            var result = string.Empty.ToByteArray(Encoding.ASCII);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((string)null).ToByteArray(Encoding.ASCII);

            var expected = new byte[0];
            Assert.Equal(expected, result);
        }
    }
}