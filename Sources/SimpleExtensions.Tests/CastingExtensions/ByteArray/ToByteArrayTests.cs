﻿using System.IO;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.ByteArray
{
    public class ToByteArrayTests
    {
        [Fact]
        public void Casting_memory_stream_should_succeed()
        {
            var array = new byte[] {0x01, 0x02, 0x03, 0x04};
            var stream = new MemoryStream(array);

            var result = stream.ToByteArray();
            
            Assert.Equal(array, result);
        }

        [Fact]
        public void Casting_empty_memory_stream_should_succeed()
        {
            var array = new byte[0];
            var stream = new MemoryStream();

            var result = stream.ToByteArray();

            Assert.Equal(array, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var array = new byte[0];

            var result = ((MemoryStream)null).ToByteArray();

            Assert.Equal(array, result);
        }
    }
}