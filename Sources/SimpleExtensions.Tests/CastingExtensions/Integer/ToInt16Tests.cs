﻿using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToInt16Tests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToInt16();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_short_should_succeed()
        {
            var result = short.MaxValue.ToString().ToInt16();

            Assert.Equal(short.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToInt16();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToInt16();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}