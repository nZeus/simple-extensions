using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToInt64NullableTests
    {
        [Fact]
        public void Casting_string_should_return_number()
        {
            var result = "1".ToInt64Nullable();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_long_should_return_number()
        {
            var result = long.MaxValue.ToString().ToInt64Nullable();

            Assert.Equal(long.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToInt64Nullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToInt64Nullable();

            Assert.Null(result);
        }
    }
}