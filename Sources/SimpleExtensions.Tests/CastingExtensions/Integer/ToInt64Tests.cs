using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToInt64Tests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToInt64();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_long_should_succeed()
        {
            var result = long.MaxValue.ToString().ToInt64();

            Assert.Equal(long.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToInt64();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToInt64();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}