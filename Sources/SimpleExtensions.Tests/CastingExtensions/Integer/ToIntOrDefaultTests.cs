using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToIntOrDefaultTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToInt(12);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToInt(12);

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToInt(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_another_string_should_return_default_value()
        {
            var result = "asd".ToInt(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToInt(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_too_big_number_should_return_default_value()
        {
            var result = long.MaxValue.ToString().ToInt(12);

            Assert.Equal(12, result);
        }
    }
}