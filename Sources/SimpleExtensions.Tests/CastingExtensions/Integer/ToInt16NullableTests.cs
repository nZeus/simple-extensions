using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToInt16NullableTests
    {
        [Fact]
        public void Casting_string_should_return_number()
        {
            var result = "1".ToInt16Nullable();

            Assert.Equal<short?>(1, result);
        }

        [Fact]
        public void Casting_max_short_should_return_number()
        {
            var result = short.MaxValue.ToString().ToInt16Nullable();

            Assert.Equal<short?>(short.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToInt16Nullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToInt16Nullable();

            Assert.Null(result);
        }
    }
}