﻿using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToIntTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToInt();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToInt();

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToInt();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToInt();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}
