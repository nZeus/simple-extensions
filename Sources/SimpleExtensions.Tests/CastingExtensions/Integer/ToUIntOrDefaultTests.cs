using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToUIntOrDefaultTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToUInt(12);

            Assert.Equal((uint)1, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = uint.MaxValue.ToString().ToUInt(12);

            Assert.Equal(uint.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToUInt(12);

            Assert.Equal((uint)12, result);
        }

        [Fact]
        public void Casting_another_string_should_return_default_value()
        {
            var result = "asd".ToUInt(12);

            Assert.Equal((uint)12, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToUInt(12);

            Assert.Equal((uint)12, result);
        }

        [Fact]
        public void Casting_too_big_number_should_return_default_value()
        {
            var result = long.MaxValue.ToString().ToUInt(12);

            Assert.Equal((uint)12, result);
        }
    }
}