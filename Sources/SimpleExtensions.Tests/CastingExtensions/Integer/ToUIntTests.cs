using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToUIntTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToUInt();

            Assert.Equal((uint)1, result);
        }

        [Fact]
        public void Casting_max_uint_should_succeed()
        {
            var result = uint.MaxValue.ToString().ToUInt();

            Assert.Equal(uint.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToUInt();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_negative_string_should_fail()
        {
            Action action = () => "-1".ToUInt();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToUInt();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}