using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToUIntNullableTests
    {
        [Fact]
        public void Casting_string_should_return_number()
        {
            var result = "1".ToUIntNullable();

            Assert.Equal((uint)1, result);
        }

        [Fact]
        public void Casting_max_unsigned_int_should_return_number()
        {
            var result = uint.MaxValue.ToString().ToUIntNullable();

            Assert.Equal(uint.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToUIntNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToUIntNullable();

            Assert.Null(result);
        }
    }
}