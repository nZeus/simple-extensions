using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToIntNullableTests
    {
        [Fact]
        public void Casting_string_should_return_number()
        {
            var result = "1".ToIntNullable();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_int_should_return_number()
        {
            var result = int.MaxValue.ToString().ToIntNullable();

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToIntNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToIntNullable();

            Assert.Null(result);
        }
    }
}