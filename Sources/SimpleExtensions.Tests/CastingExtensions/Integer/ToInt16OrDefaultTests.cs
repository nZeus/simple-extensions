using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Integer
{
    public class ToInt16OrDefaultTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "1".ToInt16(12);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_max_short_should_succeed()
        {
            var result = short.MaxValue.ToString().ToInt16(12);

            Assert.Equal(short.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToInt16(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_another_string_should_return_default_value()
        {
            var result = "asd".ToInt16(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToInt16(12);

            Assert.Equal(12, result);
        }

        [Fact]
        public void Casting_too_big_number_should_return_default_value()
        {
            var result = long.MaxValue.ToString().ToInt16(12);

            Assert.Equal(12, result);
        }
    }
}