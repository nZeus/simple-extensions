﻿using System;
using System.Collections.Generic;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Collection
{
    public class ToGroupedDictionaryTests
    {
        [Fact]
        public void Casting_list_should_succeed()
        {
            var list = new List<Tuple<int, string>>
            {
                new Tuple<int, string>(1, "One"),
                new Tuple<int, string>(1, "Another One"),
                new Tuple<int, string>(2, "Two")
            };
            var result = list.ToGroupedDictionary(x => x.Item1);

            var expected = new Dictionary<int, List<Tuple<int, string>>>
            {
                { 1, new List<Tuple<int, string>> { new Tuple<int, string>(1, "One"), new Tuple<int, string>(1, "Another One")} },
                { 2, new List<Tuple<int, string>> { new Tuple<int, string>(2, "Two")} }
            };

            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_empty_list_should_succeed()
        {
            var list = new List<Tuple<int, string>>();
            var result = list.ToGroupedDictionary(x => x.Item1);

            var expected = new Dictionary<int, List<Tuple<int, string>>>();

            Assert.Equal(expected, result);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((IList<object>)null).ToGroupedDictionary(x => x);

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}