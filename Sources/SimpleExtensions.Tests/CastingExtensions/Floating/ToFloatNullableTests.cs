using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToFloatNullableTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToFloatNullable();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToFloatNullable();

            Assert.Equal(1222333.2f, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToFloatNullable();

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToFloatNullable();

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToFloatNullable();

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_float_should_succeed()
        {
            const float value = float.MaxValue;
            var result = value.ToString("E10").ToFloatNullable();

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_return_null()
        {
            var result = "1,3.2".ToFloatNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToFloatNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToFloatNullable();

            Assert.Null(result);
        }
    }
}