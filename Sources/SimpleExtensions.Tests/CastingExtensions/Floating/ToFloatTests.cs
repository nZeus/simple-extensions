using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToFloatTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToFloat();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToFloat();

            Assert.Equal(1222333.2f, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToFloat();

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToFloat();

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToFloat();

            Assert.Equal((float)int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_float_should_succeed()
        {
            var result = float.MaxValue.ToString("E10").ToFloat();

            Assert.Equal(float.MaxValue, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_fail()
        {
            Action action = () => "1,3.2".ToFloat();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToFloat();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToFloat();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}