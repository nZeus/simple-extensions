using System;
using System.Globalization;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDoubleWithDotDecimalSeparatorTests
    {
        private const string DecimalSeparator = ".";

        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDouble(DecimalSeparator);

            Assert.Equal(1d, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDouble(DecimalSeparator);

            Assert.Equal(1222333.2d, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDouble(DecimalSeparator);

            Assert.Equal(1.2d, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_fail()
        {
            Action action = () => "1,2".ToDouble(DecimalSeparator);

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            const int value = int.MaxValue;
            var result = value.ToString().ToDouble(DecimalSeparator);

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_max_double_should_succeed()
        {
            const double value = double.MaxValue;
            var numberFormatInfo = new NumberFormatInfo {NumberDecimalSeparator = DecimalSeparator};
            var result = value.ToString("E20", numberFormatInfo).ToDouble(DecimalSeparator);

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToDouble(DecimalSeparator);

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToDouble(DecimalSeparator);

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}