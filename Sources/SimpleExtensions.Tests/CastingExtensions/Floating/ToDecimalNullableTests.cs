using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDecimalNullableTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDecimalNullable();

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDecimalNullable();

            Assert.Equal(1222333.2m, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDecimalNullable();

            Assert.Equal(1.2m, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDecimalNullable();

            Assert.Equal(1.2m, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToDecimalNullable();

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_decimal_should_succeed()
        {
            const decimal value = decimal.MaxValue;
            var result = value.ToString().ToDecimalNullable();

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_return_null()
        {
            var result = "1,3.2".ToDecimalNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToDecimalNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToDecimalNullable();

            Assert.Null(result);
        }
    }
}