using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDoubleNullableTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDoubleNullable();

            Assert.Equal(1d, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDoubleNullable();

            Assert.Equal(1222333.2d, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDoubleNullable();

            Assert.Equal(1.2d, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDoubleNullable();

            Assert.Equal(1.2d, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToDoubleNullable();

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_double_should_succeed()
        {
            const double value = double.MaxValue;
            var result = value.ToString("E20").ToDoubleNullable();

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_return_null()
        {
            var result = "1,3.2".ToDoubleNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToDoubleNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToDoubleNullable();

            Assert.Null(result);
        }
    }
}