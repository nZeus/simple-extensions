using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDecimalOrDefaultTests
    {
        private const decimal DefaultValue = 123.4m;

        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDecimal(DefaultValue);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDecimal(DefaultValue);

            Assert.Equal((decimal) 1222333.2, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDecimal(DefaultValue);

            Assert.Equal((decimal) 1.2, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDecimal(DefaultValue);

            Assert.Equal(1.2m, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToDecimal(DefaultValue);

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_decimal_should_succeed()
        {
            var result = decimal.MaxValue.ToString().ToDecimal(DefaultValue);

            Assert.Equal(decimal.MaxValue, result);
        }
        
        [Fact]
        public void Casting_string_with_comma_and_dot_should_return_default_value()
        {
            var result = "1,3.2".ToDecimal(DefaultValue);

            Assert.Equal(DefaultValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToDecimal(DefaultValue);

            Assert.Equal(DefaultValue, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToDecimal(DefaultValue);

            Assert.Equal(DefaultValue, result);
        }
    }
}