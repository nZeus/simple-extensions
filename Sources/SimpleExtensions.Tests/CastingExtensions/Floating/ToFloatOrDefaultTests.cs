using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToFloatOrDefaultTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToFloat(12f);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToFloat(12f);

            Assert.Equal(1222333.2f, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToFloat(12f);

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToFloat(12f);

            Assert.Equal(1.2f, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToFloat(12f);

            Assert.Equal((float)int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_float_should_succeed()
        {
            const float value = float.MaxValue;
            var result = value.ToString("E10").ToFloat(12f);

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_fail()
        {
            const float defaultValue = 12f;
            var result = "1,3.2".ToFloat(defaultValue);

            Assert.Equal(defaultValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            const float defaultValue = 12f;
            var result = "".ToFloat(defaultValue);

            Assert.Equal(defaultValue, result);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            const float defaultValue = 12f;
            var result = ((object)null).ToFloat(defaultValue);

            Assert.Equal(defaultValue, result);
        }
    }
}