using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDoubleOrDefaultTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDouble(12d);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDouble(12d);

            Assert.Equal(1222333.2, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDouble(12d);

            Assert.Equal(1.2, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDouble(12d);

            Assert.Equal(1.2, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var value = int.MaxValue;
            var result = value.ToString().ToDouble(12d);

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_max_double_should_succeed()
        {
            const double value = double.MaxValue;
            var result = value.ToString("E20").ToDouble(12d);

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_fail()
        {
            const double defaultValue = 12d;
            var result = "1,3.2".ToDouble(defaultValue);

            Assert.Equal(defaultValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            const double defaultValue = 12d;
            var result = "".ToDouble(defaultValue);

            Assert.Equal(defaultValue, result);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            const double defaultValue = 12d;
            var result = ((object)null).ToDouble(defaultValue);

            Assert.Equal(defaultValue, result);
        }
    }
}