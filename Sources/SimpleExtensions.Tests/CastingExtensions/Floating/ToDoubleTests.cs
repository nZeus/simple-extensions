using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDoubleTests
    {
        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDouble();

            Assert.Equal(1d, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333.2".ToDouble();

            Assert.Equal(1222333.2d, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_succeed()
        {
            var result = "1.2".ToDouble();

            Assert.Equal(1.2d, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDouble();

            Assert.Equal(1.2d, result);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var value = int.MaxValue;
            var result = value.ToString().ToDouble();

            Assert.Equal(value, result);
        }

        [Fact]
        public void Casting_max_double_should_succeed()
        {
            var result = double.MaxValue.ToString("E20").ToDouble();

            Assert.Equal(double.MaxValue, result);
        }

        [Fact]
        public void Casting_string_with_comma_and_dot_should_fail()
        {
            Action action = () => "1,3.2".ToDouble();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToDouble();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToDouble();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}