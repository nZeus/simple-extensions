using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Floating
{
    public class ToDecimalWithCommaDecimalSeparatorTests
    {
        private const string DecimalSeparator = ",";

        [Fact]
        public void Casting_integer_string_should_succeed()
        {
            var result = "1".ToDecimal(DecimalSeparator);

            Assert.Equal(1, result);
        }

        [Fact]
        public void Casting_string_with_space_as_group_separator_should_succeed()
        {
            var result = "1 222 333,2".ToDecimal(DecimalSeparator);

            Assert.Equal((decimal)1222333.2, result);
        }

        [Fact]
        public void Casting_string_with_comma_should_succeed()
        {
            var result = "1,2".ToDecimal(DecimalSeparator);

            Assert.Equal((decimal) 1.2, result);
        }

        [Fact]
        public void Casting_string_with_dot_should_fail()
        {
            Action action = () => "1.2".ToDecimal(DecimalSeparator);

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_max_int_should_succeed()
        {
            var result = int.MaxValue.ToString().ToDecimal(DecimalSeparator);

            Assert.Equal(int.MaxValue, result);
        }

        [Fact]
        public void Casting_max_decimal_should_succeed()
        {
            var result = decimal.MaxValue.ToString().ToDecimal(DecimalSeparator);

            Assert.Equal(decimal.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToDecimal(DecimalSeparator);

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToDecimal(DecimalSeparator);

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}