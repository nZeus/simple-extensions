using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Enum
{
    public class ToEnumNullableTests
    {
        private enum TestEnum
        {
            One = 1,
            Two
        }

        private struct NotEnum
        {
        }

        [Fact]
        public void Casting_proper_string_should_succeed()
        {
            var result = "Two".ToEnumNullable<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_lower_case_string_should_succeed()
        {
            var result = "two".ToEnumNullable<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_integer_should_succeed()
        {
            var result = 2.ToEnumNullable<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_incorrect_string_should_fail()
        {
            Action action = () => "incorrect".ToEnumNullable<TestEnum>();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToEnumNullable<TestEnum>();

            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void Casting_to_incorrect_type_should_fail()
        {
           Action action = () => "whatever_here".ToEnumNullable<NotEnum>();

           Assert.Throws<ArgumentException>(action);
        }
    }
}
