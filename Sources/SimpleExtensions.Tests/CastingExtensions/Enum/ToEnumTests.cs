using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Enum
{
    public class ToEnumTests
    {
        private enum TestEnum
        {
            One = 1,
            Two
        }

        private struct NotEnum
        {
        }

        [Fact]
        public void Casting_proper_string_should_succeed()
        {
            var result = "Two".ToEnum<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_lower_case_string_should_succeed()
        {
            var result = "two".ToEnum<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_integer_should_succeed()
        {
            var result = 2.ToEnum<TestEnum>();

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_incorrect_string_should_fail()
        {
            Action action = () => "incorrect".ToEnum<TestEnum>();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToEnum<TestEnum>();

            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void Casting_to_incorrect_type_should_fail()
        {
           Action action = () => "whatever_here".ToEnum<NotEnum>();

           Assert.Throws<ArgumentException>(action);
        }
    }
}
