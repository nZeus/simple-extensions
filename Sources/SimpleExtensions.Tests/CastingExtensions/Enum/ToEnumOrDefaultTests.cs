using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Enum
{
    public class ToEnumOrDefaultTests
    {
        private enum TestEnum
        {
            One = 1,
            Two,
            Three
        }

        private struct NotEnum
        {
        }

        [Fact]
        public void Casting_proper_string_should_succeed()
        {
            var result = "Two".ToEnum<TestEnum>(TestEnum.Three);

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_lower_case_string_should_succeed()
        {
            var result = "two".ToEnum<TestEnum>(TestEnum.Three);

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_integer_should_succeed()
        {
            var result = 2.ToEnum<TestEnum>(TestEnum.Three);

            Assert.Equal(TestEnum.Two, result);
        }

        [Fact]
        public void Casting_incorrect_string_should_succeed()
        {
            var result = "asl".ToEnum<TestEnum>(TestEnum.Three);

            Assert.Equal(TestEnum.Three, result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((object)null).ToEnum<TestEnum>(TestEnum.Three);

            Assert.Equal(TestEnum.Three, result);
        }

        [Fact]
        public void Casting_incorrect_type_should_fail()
        {
            Action action = () => "asd".ToEnum<NotEnum>(new NotEnum());

            Assert.Throws<ArgumentException>(action);
        }
    }
}
