using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.String
{
    public class ToStringSafeTests
    {
        [Fact]
        public void Casting_non_empty_value_to_string_should_succeed()
        {
            var result = 12.ToStringSafe();

            Assert.Equal("12", result);
        }

        [Fact]
        public void Casting_empty_string_should_return_empty_string()
        {
            var result = string.Empty.ToStringSafe();

            Assert.Equal(string.Empty, result);
        }

        [Fact]
        public void Casting_null_should_return_empty_string()
        {
            var result = ((object)null).ToStringSafe();

            Assert.Equal(string.Empty, result);
        }
    }
}