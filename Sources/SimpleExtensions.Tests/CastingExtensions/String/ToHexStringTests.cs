using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.String
{
    public class ToHexStringTests
    {
        [Fact]
        public void Casting_byte_array_to_string_should_succeed()
        {
            var result = new byte[] {0xfe, 0x3a}.ToHexString();

            Assert.Equal("FE3A", result);
        }

        [Fact]
        public void Casting_empty_byte_array_to_string_should_succeed()
        {
            var result = new byte[] {}.ToHexString();

            Assert.Equal("", result);
        }
        
        [Fact]
        public void Casting_null_byte_array_to_string_should_succeed()
        {
            var result = ((byte[])null).ToHexString();

            Assert.Equal("", result);
        }
    }
}