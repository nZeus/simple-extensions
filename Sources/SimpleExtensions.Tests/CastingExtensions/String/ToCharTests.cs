using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.String
{
    public class ToCharTests
    {
        [Fact]
        public void Casting_one_char_string_should_succeed()
        {
            var result = "a".ToChar();

            Assert.Equal('a', result);
        }

        [Fact]
        public void Casting_two_chars_string_should_fail()
        {
            Action action = () => "ab".ToChar();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToChar();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToChar();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}