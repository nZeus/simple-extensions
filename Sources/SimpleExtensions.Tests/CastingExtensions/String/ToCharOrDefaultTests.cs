using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.String
{
    public class ToCharOrDefaultTests
    {
        [Fact]
        public void Casting_one_char_string_should_succeed()
        {
            var result = "a".ToChar('b');

            Assert.Equal('a', result);
        }

        [Fact]
        public void Casting_two_chars_string_should_return_default_value()
        {
            var result = "ab".ToChar('b');

            Assert.Equal('b', result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToChar('b');

            Assert.Equal('b', result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToChar('b');

            Assert.Equal('b', result);
        }
    }
}