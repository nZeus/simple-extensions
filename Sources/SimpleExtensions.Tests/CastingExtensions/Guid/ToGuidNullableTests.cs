using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Guid
{
    public class ToGuidNullableTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var guid = System.Guid.NewGuid();
            var result = guid.ToString().ToGuidNullable();

            Assert.Equal(guid, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            var result = "".ToGuidNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            var result = ((object)null).ToGuidNullable();

            Assert.Null(result);
        }
    }
}