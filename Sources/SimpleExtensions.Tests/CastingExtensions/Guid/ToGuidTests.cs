using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Guid
{
    public class ToGuidTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var guid = System.Guid.NewGuid();
            var result = guid.ToString().ToGuid();

            Assert.Equal(guid, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToGuid();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToGuid();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}