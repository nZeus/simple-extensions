using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Guid
{
    public class ToGuidOrDefaultTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var guid = System.Guid.NewGuid();
            var result = guid.ToString().ToGuid(System.Guid.Empty);

            Assert.Equal(guid, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var result = "".ToGuid(System.Guid.Empty);

            Assert.Equal(System.Guid.Empty, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var result = ((object)null).ToGuid(System.Guid.Empty);

            Assert.Equal(System.Guid.Empty, result);
        }
    }
}