﻿using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Boolean
{
    public class ToBoolTests
    {
        [Fact]
        public void Casting_true_string_should_succeed()
        {
            var result = "true".ToBool();

            Assert.True(result);
        }

        [Fact]
        public void Casting_false_string_should_succeed()
        {
            var result = "false".ToBool();

            Assert.False(result);
        }

        [Fact]
        public void Casting_string_number_should_fail()
        {
            Action action = () => "1".ToBool();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToBool();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToBool();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}