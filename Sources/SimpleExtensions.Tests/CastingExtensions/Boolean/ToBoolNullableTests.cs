using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Boolean
{
    public class ToBoolNullableTests
    {
        [Fact]
        public void Casting_true_string_should_succeed()
        {
            var result = "true".ToBoolNullable();

            Assert.Equal(true, result);
        }

        [Fact]
        public void Casting_false_string_should_succeed()
        {
            var result = "false".ToBoolNullable();

            Assert.Equal(false, result);
        }

        [Fact]
        public void Casting_string_number_should_return_null()
        {
            var result = "1".ToBoolNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToBoolNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToBoolNullable();

            Assert.Null(result);
        }
    }
}