using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.Boolean
{
    public class ToBoolOrDefaultTests
    {
        [Fact]
        public void Casting_true_string_should_succeed()
        {
            var result = "true".ToBool(false);

            Assert.True(result);
        }

        [Fact]
        public void Casting_false_string_should_succeed()
        {
            var result = "false".ToBool(true);

            Assert.False(result);
        }

        [Fact]
        public void Casting_string_number_should_succeed()
        {
            var result = "1".ToBool(false);

            Assert.False(result);
        }

        [Fact]
        public void Casting_empty_string_should_succeed()
        {
            var result = "".ToBool(true);

            Assert.True(result);
        }

        [Fact]
        public void Casting_null_should_succeed()
        {
            var result = ((object)null).ToBool(true);

            Assert.True(result);
        }
    }
}