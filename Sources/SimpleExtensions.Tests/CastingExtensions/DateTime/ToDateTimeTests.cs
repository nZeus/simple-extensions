using System;
using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.DateTime
{
    public class ToDateTimeTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "2015/12/31".ToDateTime();

            Assert.Equal(new System.DateTime(2015, 12, 31), result);
        }

        [Fact]
        public void Casting_min_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MinValue.ToString("O").ToDateTime();

            Assert.Equal(System.DateTime.MinValue, result);
        }

        [Fact]
        public void Casting_min_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MinValue).ToDateTime();

            Assert.Equal(System.DateTime.MinValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MaxValue.ToString("O").ToDateTime();

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MaxValue).ToDateTime();

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_fail()
        {
            Action action = () => "".ToDateTime();

            Assert.Throws<InvalidCastException>(action);
        }

        [Fact]
        public void Casting_null_should_fail()
        {
            Action action = () => ((object)null).ToDateTime();

            Assert.Throws<ArgumentNullException>(action);
        }
    }
}