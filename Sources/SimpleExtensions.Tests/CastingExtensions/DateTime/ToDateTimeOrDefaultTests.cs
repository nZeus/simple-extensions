using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.DateTime
{
    public class ToDateTimeOrDefaultTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "2015/12/31".ToDateTime(System.DateTime.MinValue);

            Assert.Equal(new System.DateTime(2015, 12, 31), result);
        }

        [Fact]
        public void Casting_min_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MinValue.ToString("O").ToDateTime(System.DateTime.MaxValue);

            Assert.Equal(System.DateTime.MinValue, result);
        }

        [Fact]
        public void Casting_min_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MinValue).ToDateTime(System.DateTime.MaxValue);

            Assert.Equal(System.DateTime.MinValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MaxValue.ToString("O").ToDateTime(System.DateTime.MinValue);

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MaxValue).ToDateTime(System.DateTime.MinValue);

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_empty_string_should_return_default_value()
        {
            var defaultValue = System.DateTime.MinValue;

            var result = "".ToDateTime(defaultValue);

            Assert.Equal(defaultValue, result);
        }

        [Fact]
        public void Casting_null_should_return_default_value()
        {
            var defaultValue = System.DateTime.MinValue;

            var result = ((object)null).ToDateTime(defaultValue);

            Assert.Equal(defaultValue, result);
        }
    }
}