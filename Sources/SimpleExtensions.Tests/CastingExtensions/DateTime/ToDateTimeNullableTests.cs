using Xunit;

namespace SimpleExtensions.Tests.CastingExtensions.DateTime
{
    public class ToDateTimeNullableTests
    {
        [Fact]
        public void Casting_string_should_succeed()
        {
            var result = "2015/12/31".ToDateTimeNullable();

            Assert.Equal(new System.DateTime(2015, 12, 31), result);
        }

        [Fact]
        public void Casting_min_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MinValue.ToString("O").ToDateTimeNullable();

            Assert.Equal(System.DateTime.MinValue, result);
        }
        
        [Fact]
        public void Casting_min_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MinValue).ToDateTimeNullable();

            Assert.Equal(System.DateTime.MinValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_string_should_succeed()
        {
            var result = System.DateTime.MaxValue.ToString("O").ToDateTimeNullable();

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_max_DateTime_as_object_should_succeed()
        {
            var result = ((object)System.DateTime.MaxValue).ToDateTimeNullable();

            Assert.Equal(System.DateTime.MaxValue, result);
        }

        [Fact]
        public void Casting_wrong_string_should_return_null()
        {
            var result = "WRONG_STRING".ToDateTimeNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_empty_string_should_return_null()
        {
            var result = "".ToDateTimeNullable();

            Assert.Null(result);
        }

        [Fact]
        public void Casting_null_should_return_null()
        {
            var result = ((object)null).ToDateTimeNullable();

            Assert.Null(result);
        }
    }
}