﻿using System;
using System.Collections.Generic;
using Xunit;

namespace SimpleExtensions.Tests.CheckingExtensions
{
    public class DictionaryExtensionsTests
    {
        [Fact]
        public void Getting_a_value_of_a_value_type_by_a_key_that_exist_should_succeed()
        {
            var dictionary = new Dictionary<string, int> {
                { "one", 1 }
            };

            var result = dictionary.GetValueSafe("one");

            Assert.Equal(1, result);
        }

        [Fact]
        public void Getting_a_value_of_a_value_type_by_a_key_that_does_not_exist_should_not_fail_and_return_null()
        {
            var dictionary = new Dictionary<string, int> {
                { "one", 1 }
            };

            int? result = dictionary.GetValueSafe("two");

            Assert.Null(result);
        }

        [Fact]
        public void Getting_a_value_of_a_reference_type_by_a_key_that_exist_should_succeed()
        {
            var dictionary = new Dictionary<string, string> {
                { "one", "One" }
            };

            var result = dictionary.GetValueSafe("one");

            Assert.Equal("One", result);
        }

        [Fact]
        public void Getting_a_value_of_a_reference_type_by_a_key_that_does_not_exist_should_not_fail_and_return_null()
        {
            var dictionary = new Dictionary<string, string> {
                { "one", "One" }
            };

            string result = dictionary.GetValueSafe("two");

            Assert.Null(result);
        }

        [Fact]
        public void Getting_a_value_of_a_reference_type_by_a_key_that_does_not_exist_should_not_fail_and_return_default_value()
        {
            var dictionary = new Dictionary<string, string> {
                { "one", "One" }
            };

            string result = dictionary.GetValueSafe("two", "Two");

            Assert.Equal("Two", result);
        }
    }
}
